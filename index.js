const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoute = require("./routes/userRoutes");
const productRoute = require("./routes/productRoutes");


const app = express();
const port = process.env.PORT || 2000;

mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://Ricowafu:admin123@224-rico.tipkxxe.mongodb.net/Ecommerce?retryWrites=true&w=majority",

		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}

	);

let db = mongoose.connection;

db.on("error", () => console.error("Connect error."));
db.once("open", () => console.log("Connected to MongoDB"));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));





// Main URI
app.use("/users", userRoute);
app.use("/products", productRoute);

app.listen(port, () => {console.log(`API is running at port ${port}`)});