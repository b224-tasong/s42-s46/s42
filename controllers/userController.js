const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// ========== Register User =================================

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)

	})
	
	return newUser.save().then((result, error) => {
		if (error) {
			return false
		}else{
			return true
		}
	})
};


//================= User Login ===============================
 
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if (result === null) {
			return false
		}else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if (isPasswordCorrect) {
				return {tokenAccess: auth.createAccessToken(result)}
			}else{
				return false
			}
		}	
	})
};


module.exports.isUserAdmin = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result === null) {
			return false
		}else{
			return result
		}
	})
}




//================= Check Duplicate Emails ===============================
module.exports.checkDuplicateEmail = (reqBody, res) => {

	return User.find({email: reqBody.email}).then(result => {
			
			// The "find" method returns a record if a match is found
			if(result.length > 0) {
				return true
			// No duplicate email found
			// The user is not yet registered in the database
			} else if (result.length == 0) {
				return false
			}
		})
		
};


//==================== Create Checkout Order ==========================

module.exports.checkOutProduct = async (userData, reqBody) => {
	let user_id = userData.id;
	let total;

	console.log(userData)
	console.log(reqBody)

	let isUserUpdated = await User.findById(userData.id).then(userResult => {

		if (userResult === null) {
			return false;
		}



		return Product.findById(reqBody.productId).then(getProdDataResult => {

			if (getProdDataResult.isActive === false) {
				return false
			}else{
				userResult.orderedProduct.push({
					products: [
							{
								productId: reqBody.productId,
								productName: getProdDataResult.name,
								quantity: reqBody.quantity
							}
						],
					totalAmount: total = getProdDataResult.price * reqBody.quantity
				});
				return userResult.save().then((result, error) => {
					if (error) {
						return false
					}else{
						return true
					}
				})
			}	
		})
	});


	let isProductUpdated = await Product.findById(reqBody.productId).then(prodResult => {

		console.log(prodResult)
		if (prodResult == null) {
			return false;
		}
		return User.findById(userData.id).then(userResult => {

			console.log(userResult)
			let orderId = userResult.orderedProduct[userResult.orderedProduct.length-1]._id
			
			if (prodResult.isActive) {
				prodResult.userOrder.push({
					userId: userData.id,
					orderId: orderId
				});


				return prodResult.save().then((result, error) => {
					if (error) {
						return false
					}else{
						return true
					}
				})

			}else{

				return false
			}
		})
	});


	if (isUserUpdated && isProductUpdated) {

		return true

	}else{
		
		return false
	}                                              
};




//==================== Retrieve User Details ==========================

module.exports.retrieveUserDetails = (userData) => {

	return User.findById(userData.id).then(result => {
	
		return result
	});
};


module.exports.retrieveUserInfo = (reqParams) => {

	return User.findById(reqParams.id).then(result => {
	
		return result
	});
};



module.exports.retrieveAllUserDetails = () => {

	return User.find({}).then(result => {
	
		return result
	});
};



//================= Update User Details ===============================

module.exports.updateUserData = (reqParams, reqBody) => {

	let updateInfo = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo
	}
	// console.log(updateInfo)
	// console.log(reqParams.id)

	return User.findByIdAndUpdate(reqParams.id, updateInfo).then((result, error) =>{
		if (error) {
			return false
		}else{
			return true
		}
	})
};



//================= Update User Password ===============================


module.exports.comparePassword = (reqParams ,reqBody) =>{

	return User.findById(reqParams.id).then(result =>{
		if (result === null) {
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 
			if (isPasswordCorrect) {

				return true

			}else{

				return false
			}
		}
	})
};



module.exports.updatePassword = (reqParams ,reqBody) =>{

	return User.findById(reqParams.id).then(result =>{
		if (result === null) {
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password1, result.password) 
			if (isPasswordCorrect) {
				let passwordNew = {
					password: bcrypt.hashSync(reqBody.password, 10)
				}
				 return User.findByIdAndUpdate(reqParams.id, passwordNew).then((result, error) => {
				 	if (error) {
				 		return false
				 	}else{
				 		return true
				 	}
				 })
			}else{
				return false
			}
		}
	})
};


//==================== Set User as an Admin ==========================

module.exports.setUserAsAdmin = (reqParams) => {
	let updateUserRole = {

		isAdmin: true

	}

	return User.findByIdAndUpdate(reqParams.id, updateUserRole).then((result, error) => {

		if (error) {
			return	false
		}else{
			return true
		}

	});
};


module.exports.setUser = (reqParams) => {
	let updateUserRole = {

		isAdmin: false

	}

	return User.findByIdAndUpdate(reqParams.id, updateUserRole).then((result, error) => {

		if (error) {
			return	false
		}else{
			return true
		}

	});
};



//==================== Retrieve All Orders ==========================

module.exports.getAllOrders = () => {

	return User.find({}).then(result => {
		let allOrders = [];
		result.forEach(user => {
			user.orderedProduct.forEach(order => {
				let orderDetails = {
					"products": order.products,
					"totalAmount": order.totalAmount,
					"purchasedOn": order.purchasedOn
				}
				allOrders.push(orderDetails);
			});
		});
		return allOrders;
	})
}





//==================== Retrieve Authenticated User Order ==========================

module.exports.retrieveUserOrderDetails = (userData) => {

	return User.findById(userData.id).then(result => {

		return `Ordered Details: \n
				${result.orderedProduct}\n`
	});
};



//==================== Add to Cart ==========================
let prodArray = [];
let userArray = [];
module.exports.addToCart = (prodData) => {
	

	return Product.findById(prodData.productId).select("name price descr").then(prodResult => {
		
		
		if (prodResult.isActive == false) {
			return false
		}else{
			prodResult.price *= prodData.quantity
			prodArray.push({prodResult});

			return prodArray
		}
	})	
};


//==================== Add to Cart ==========================

module.exports.saveToCartInfo = (item) => {
	return Product.findById(item.productId).then(result	=>{
		if (result.isActive	=== false) {
			return	false	
		}else{
			prodArray.push({item})

			return prodArray
		}
	})
}


// module.exports.getCartData = async (item) => {
// 	console.log(prodArray)
	
// 	// Find the item in the prodArray that matches the id from item.userId
// 	let match = prodArray.filter(prod => prod.item.userId === item.id);
// 	if (match.length > 0) {
// 		let updatedArray = match.map(item => {
// 			return {
// 				...item, 
// 				quantity: item.item.quantity
// 			};
// 		});
// 		let prodIdArray = updatedArray.map(item => item.item.productId);
// 		try {
// 			let products = await Product.find({'_id': {$in: prodIdArray}});
// 			// Add the quantity field to the products
// 			let newProducts = products.map(product => {
// 				let item = updatedArray.find(item => item.item.productId === product._id.toString());
// 				return {
// 					...product.toObject(),
// 					quantity: item.quantity
// 				};
// 			});
// 			return newProducts;
// 		} catch(err) {
// 			console.log(err);
// 			return Promise.reject(err);
// 		}
// 	}
// };






// module.exports.getCartData = async (item) => {
// 	let updatedArray = prodArray.map(item => {
// 		return {
// 			...item, 
// 			quantity: item.item.quantity
// 		};
// 	});

// 	// Find the products in the database with the same productIds as the items in the updatedArray
// 	let prodIdArray = updatedArray.map(item => item.item.productId);
// 	try {
// 		let products = await Product.find({'_id': {$in: prodIdArray}});
// 		// Add the quantity field to the products
// 		let newProducts = products.map(product => {
// 			let item = updatedArray.find(item => item.item.productId === product._id.toString());
// 			return {
// 				...product._doc,
// 				quantity: item.quantity
// 			}
// 		});
// 		return newProducts;
// 	} catch(err) {
// 		console.log(err);
// 		return Promise.reject(err);
// 	}
// };




module.exports.getCartData = async (item) => {
	console.log(prodArray)
	
	// Find the item in the prodArray that matches the id from item.userId
	let match = prodArray.filter(prod => prod.item.userId === item.id);
	if (match.length > 0) {
		let prodIdArray = match.map(item => item.item.productId);
		try {
			let products = await Product.find({'_id': {$in: prodIdArray}});
			return products;
		} catch(err) {
			console.log(err);
			return Promise.reject(err);
		}
	}
};