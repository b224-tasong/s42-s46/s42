const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Product description is required"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required"]
	},
	imageLink: {
	type: String,
	required: [true, "Image link is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
	default: new Date()
	},
	userOrder: [

			{
				userId: {
					type: String,
					required: [true, "User ID is required"]
				},
				orderId:{
					type: String,
					required: [true, "Order ID is required"]
				}
			}
		]

});



module.exports = mongoose.model("Product", productSchema);