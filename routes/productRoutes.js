const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


//================= Create Product ===============================


router.post("/", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)
	
	let data = {
		name: req.body.name,
		description: req.body.description,
		imageLink: req.body.imageLink,
		price: req.body.price
	}

	if (userData.isAdmin === false) {
		res.send({auth: "Only admin can add products Info"})
	}else{
		productController.createProduct(data).then(result => res.send(result))
	}
	
	
});


//================= Update Product Info ===============================

router.put("/:id", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {
		productController.updateProductInfo(req.params,req.body).then(result => res.send(result))
	}else{
		res.send({auth: "Only admin can update products Info"})
	}
});







//================= ARCHIVE Product Info ===============================


router.patch("/archive/:id", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {
		
		productController.ArchiveProduct(req.params).then(result => res.send(result))

	}else{
		res.send({auth: "Only admin can update products Info"})
	}
});

router.patch("/active/:id", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {
		
		productController.activateProduct(req.params).then(result => res.send(result))

	}else{
		res.send({auth: "Only admin can update products Info"})
	}
});



//================= Delete Product Info ===============================

// router.delete("/:id", auth.verify, (req, res) => {
// 	let userData = auth.decode(req.headers.authorization)

// 	if (userData.isAdmin == true) {
// 		productController.deleteProdInfo(req.params.id).then(result => res.send(result))
// 	}else{
// 		res.send({auth: "Only admin can delete products Info"})
// 	}
// });

//================== Retrieve all products by admin ========================

router.get("/allProducts", auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === false) {

		res.send({auth: "failed"})

	}else{
		productController.adminGetAllProduct().then(result => res.send(result))
	}
});


// ================ Get Specific Product Info ==========================
router.get("/singleProduct/:prodId", (req, res) => {

	productController.getSpecificProduct(req.params).then(result => res.send(result))
});







//================= GET all active products ===============================

router.get("/", (req, res) => {
	productController.getActiveProducts().then(result => res.send(result))
});










module.exports = router ; 