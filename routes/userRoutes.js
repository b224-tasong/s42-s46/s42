const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// ========== Register User =================================

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});



//================= User Login ===============================

router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(result => res.send(result))
	
});


router.post("/checkAdmin", (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.isUserAdmin(req.body).then(result => res.send(result))
});


//================= Check Duplicate Emails ===============================
router.post("/checkEmail", auth.verify, (req, res) =>{
	// console.log(req.body)
	userController.checkDuplicateEmail(req.body, res).then(result => res.send(result))
});


//==================== Create Checkout Order ==========================

router.post("/order", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)
	// console.log(userData)
	// console.log(req.body)
	// let prodData = {
	// 	productId: req.body.productId,
	// 	quantity: req.body.quantity
	// }


	if (userData.isAdmin === false) {

		userController.checkOutProduct(userData, req.body).then(result => res.send(result))

	}else if (userData.isAdmin === true){

		res.send({auth: "Please use a user account for this function"})

	}else{
		res.send({auth})
	}
	
	
});


//==================== Retrieve User Details ==========================

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

		userController.retrieveUserDetails(userData).then(result => res.send(result))	
	
});

router.get("/userDetails/:id",(req, res) => {


		userController.retrieveUserInfo(req.params).then(result => res.send(result))	
	
});

router.get("/allDetails", (req, res) => {

		userController.retrieveAllUserDetails().then(result => res.send(result))	
	
});



//======================Update user details ==========================//
router.put("/updateUserInfo/:id", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {
		userController.updateUserData(req.params,req.body).then(result => res.send(result))
	}else{
		res.send({auth: "Only admin can update products Info"})
	}
});



//====================== Update Password ==========================//
router.post("/comparePassword/:id", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {
		userController.comparePassword(req.params,req.body).then(result => res.send(result))
	}else{
		res.send({auth: "Only admin can update products Info"})
	}
});



router.patch("/password/:id", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {
		userController.updatePassword(req.params,req.body).then(result => res.send(result))
	}else{
		res.send({auth: "Only admin can update products Info"})
	}
});


//==================== Set User as an Admin ==========================

router.patch("/setAdmin/:id", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == false) {

		res.send({auth: "You are not an admin"})
	
	}else{

		userController.setUserAsAdmin(req.params, req.body).then(result => res.send(result))
	}
});

router.patch("/setUser/:id", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == false) {

		res.send({auth: "You are not an admin"})
	
	}else{

		userController.setUser(req.params).then(result => res.send(result))
	}
});


//==================== Retrieve All Orders ==========================

router.get("/allOrders", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == false) {

		res.send({auth: "You are not an admin"})
	
	}else{

		userController.getAllOrders().then(result => res.send(result))
	}
});


//==================== Retrieve Authenticated User Order ==========================

router.get("/AuthenticatedUserDetails", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == false) {

		userController.retrieveUserOrderDetails(userData).then(result => res.send(result))	
	}else{
		res.send({auth: "Please use a user account for this function"})
	}
});


//==================== Add to Cart ==========================

// router.post("/cart", auth.verify, (req, res) => {
// 	let userData = auth.decode(req.headers.authorization)
// 	console.log(userData)
// 	let prodData = {
// 		userId: userData.id,
// 		productId: req.body.productId,
// 		quantity: req.body.quantity
// 	}

// 	if (userData.isAdmin == false) {

// 		userController.addToCart(prodData).then(result => res.send(result))	
// 	}else{
// 		res.send({auth: "Please use a user account for this function"})
// 	}
// });



router.post("/cart", auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)
	let cartInfo = {
		userId: req.body.id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	if (userData.isAdmin === false) {

		userController.saveToCartInfo(cartInfo).then(result=> res.send(result))
	}else{
		res.send({auth})
	}

});



router.post("/cartData", auth.verify, (req, res) =>{
	let userData = auth.decode(req.headers.authorization)


	let item = {
		userId: userData.id,
		productId: req.body.productId,
	}


	if (userData.isAdmin === false) {

		userController.saveToCartInfo(item).then(result=> res.send(result))	

	}else{
		res.send({auth})
	}

});



router.get("/getCartDetails", auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)
	 
	if (userData.isAdmin === false) {

		userController.getCartData(userData).then(result => res.send(result))

	}else{

		res.send({auth})
	}
});



module.exports = router ; 
